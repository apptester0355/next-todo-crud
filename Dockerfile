FROM node:alpine

WORKDIR /app

COPY ./package*.json ./
COPY ./prisma prisma

RUN npm install

COPY . .

ENV POSTGRES_PRISMA_URL=${POSTGRES_PRISMA_URL}
ENV POSTGRES_URL=${POSTGRES_URL}
ENV PATH /app/node_modules/.bin:$PATH

EXPOSE ${PORT}

CMD ["npm", "run","dev"]
