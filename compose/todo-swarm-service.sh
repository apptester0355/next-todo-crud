#!/bin/bash

service_name="todo-crud"
docker service rm $service_name

# Check if the service exists
if docker service ls --format '{{.Name}}' | grep -wq $service_name; then
    echo "Service $service_name exists. Removing it..."
    docker service rm $service_name
else
    echo "Service $service_name does not exist."
fi

# Create the service
echo "Creating service $service_name..."
docker service create \
  --env POSTGRES_PRISMA_URL=$POSTGRES_URL \
  --env POSTGRES_URL=$POSTGRES_URL \
  --name todo-crud \
  --network my-network \
  --publish published=$PORT,target=3000 \
  $REGISTRY_USER/$IMAGE_NAME:2.0