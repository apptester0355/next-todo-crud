#!/bin/bash

docker network create --driver overlay my-network
docker service rm postgres-db

CONFIG_NAME="init-sql"
CONFIG_FILE="./init.sql"
VOLUME_NAME="pg-data"

# Check and create init.sql if it does not exist
if [ ! -f "${CONFIG_FILE}" ]; then
  cat <<EOL > ${CONFIG_FILE}
CREATE TABLE "Task" (
  "id" SERIAL NOT NULL,
  "title" TEXT NOT NULL,
  "description" TEXT,
  "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "Task_pkey" PRIMARY KEY ("id")
);
EOL
  echo "${CONFIG_FILE} has been created with initial SQL data."
else
  echo "${CONFIG_FILE} already exists."
fi

# Check and create config if it does not exist
if ! docker config ls --format '{{.Name}}' | grep -q "^${CONFIG_NAME}$"; then
  docker config create ${CONFIG_NAME} ${CONFIG_FILE}
else
  echo "Config ${CONFIG_NAME} already exists."
fi

# Check and create volume if it does not exist
if ! docker volume ls --format '{{.Name}}' | grep -q "^${VOLUME_NAME}$"; then
  docker volume create ${VOLUME_NAME}
else
  echo "Volume ${VOLUME_NAME} already exists."
fi


docker service create \
  --env POSTGRES_USER=$REGISTRY_USER \
  --env POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  --env POSTGRES_DB=$POSTGRES_DB \
  --network my-network \
  --name postgres-db \
  --publish published=5432,target=5432 \
  --mount type=volume,source=pg-data,target=/var/lib/postgresql/data \
  --config source=init-sql,target=/docker-entrypoint-initdb.d/init.sql \
  postgres:$PS_IMAGE_TAG