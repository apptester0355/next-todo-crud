# !/bin/bash
##### Install Docker for Ubuntu#####
sudo apt update
sudo apt install -y docker.io
sudo apt install docker-compose-v2
PRIVATE_IP=$(hostname -I | awk '{print $3}')
echo $PRIVATE_IP
sudo docker swarm init --advertise-addr $PRIVATE_IP
touch /tmp/swarm.sh
echo "#! /bin/bash" > /tmp/swarm.sh
docker swarm join-token worker | grep "docker swarm join" | awk '{$1=$1;print}' >> /tmp/swarm.sh

