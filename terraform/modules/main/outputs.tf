output "droplet_ip" {
  value = digitalocean_droplet.manager-droplet.ipv4_address
}