terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.38.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "key_01" {
  name       = "my-ssh"
  public_key = file("${var.ssh_private_key_path}.pub")
}

