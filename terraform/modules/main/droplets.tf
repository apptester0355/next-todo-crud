###############################
#Manager node
###############################
resource "digitalocean_droplet" "manager-droplet" {
  image  = "ubuntu-24-04-x64"
  name   = var.name
  region = "nyc3"
  size   = "s-2vcpu-4gb"
  ssh_keys = [digitalocean_ssh_key.key_01.fingerprint]


  provisioner "file" {
    source      = "${var.file_path}/manager_swarm.sh"
    destination = "/tmp/manager_swarm.sh"
  }

 provisioner "remote-exec" {
   inline = [
     "chmod +x /tmp/manager_swarm.sh",
     "/tmp/manager_swarm.sh args",
     
   ]
 }
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no root@${self.ipv4_address}:/tmp/swarm.sh ./scripts/swarm.sh"
  }
    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_private_key_path)
      host        = self.ipv4_address
    }
}


#####################################
# worker node
#####################################
resource "digitalocean_droplet" "worker-droplet" {
  image  = "ubuntu-24-04-x64"
  name   = var.name2
  region = "nyc3"
  size   = "s-2vcpu-4gb"
  ssh_keys = [digitalocean_ssh_key.key_01.fingerprint]

  provisioner "file" {
    source      = "${var.file_path}/docker.sh"
    destination = "/tmp/docker.sh"
  }

  provisioner "file" {
    source      = "${var.file_path}/swarm.sh"
    destination = "/tmp/swarm.sh"
  }

 provisioner "remote-exec" {
   inline = [
     "chmod +x /tmp/docker.sh",
     "chmod a+x /tmp/swarm.sh",
     "/tmp/docker.sh args",
     "/tmp/swarm.sh args",
     
   ]
 }

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_private_key_path)
      host        = self.ipv4_address
    }
    depends_on = [digitalocean_droplet.manager-droplet]
    
}

# ########################################
# # monitoring  node
# #######################################

resource "digitalocean_droplet" "monitoring-droplet" {
  image  = "ubuntu-24-04-x64"
  name   = var.name3
  region = "nyc3"
  size   = "s-2vcpu-4gb"
  ssh_keys = [digitalocean_ssh_key.key_01.fingerprint]

  provisioner "file" {
    source      = "${var.file_path}/docker.sh"
    destination = "/tmp/docker.sh"
  }

 provisioner "remote-exec" {
   inline = [
     "chmod +x /tmp/docker.sh",
     "/tmp/docker.sh args",
      "git clone https://github.com/vegasbrianc/prometheus.git",
      "cd prometheus",
      "docker compose -f docker-compose.yml up -d"
   ]
 }

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_private_key_path)
      host        = self.ipv4_address
    }
}

########################################
resource "digitalocean_droplet" "testing-droplet" {
  image  = "ubuntu-24-04-x64"
  name   = var.name4
  region = "nyc3"
  size   = "s-2vcpu-4gb"
  ssh_keys = [digitalocean_ssh_key.key_01.fingerprint]

  provisioner "file" {
    source      = "${var.file_path}/docker.sh"
    destination = "/tmp/docker.sh"
  }

 provisioner "remote-exec" {
   inline = [
     "chmod +x /tmp/docker.sh",
     "/tmp/docker.sh args",
      
   ]
 }

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_private_key_path)
      host        = self.ipv4_address
    }
}