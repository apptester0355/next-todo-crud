variable "do_token" {
  type = string
}
variable "ssh_private_key_path" {
  description = "Path to the private SSH key"
  type        = string
}

variable "name" {
  description = "name of manager node for docker swarm"
  type        = string
}

variable "name2" {
  description = "name of worker node for docker swarm"
  type        = string
}

variable "name3" {
  description = "name of monitoring node"
  type        = string
}

variable "name4" {
  description = "name of testing node"
  type        = string
}

variable "prometheus" {
  description = "docker compose file for prometheus"
  type = string
}

variable "file_path" {
  description = "Path to executable file named docker.sh"  
  type        = string
}

# variable "cmd" {
#   description = "local exec command"
# }