terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.38.0"
    }
  }
}
provider "digitalocean" {
  token = var.do_token
}

# module "key_01" {
#   source = "./modules/"
# }

module "droplets" {
  source = "./modules/main"
  ssh_private_key_path = var.ssh_private_key_path
  do_token = var.do_token
  name = var.name
  name2 = var.name2
  name3 = var.name3
  name4 = var.name4
  file_path = var.file_path  
  prometheus = var.prometheus
  #cmd = var.cmd
}