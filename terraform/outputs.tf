output "public_ip" {
  value = module.droplets.droplet_ip
  description = "Ip address of the Droplet"
}